# Remote storage (S3)

PeerTube supports streaming directly from an s3 public bucket. The integration
is done via FUSE, for instance with [s3fs](https://github.com/s3fs-fuse/s3fs-fuse).

```bash
export S3_STORAGE=/var/www/peertube/s3-storage
mkdir $S3_STORAGE
mount --bind /var/www/peertube/storage/videos $S3_STORAGE/videos
mount --bind /var/www/peertube/storage/redundancy $S3_STORAGE/redundancy
mount --bind /var/www/peertube/storage/streaming-playlists $S3_STORAGE/streaming-playlists
s3fs your-space-name /var/www/peertube/s3-storage -o url=https://region.digitaloceanspaces.com -o allow_other -o use_path_request_style -o uid=1000 -o gid=1000
```

_note_: see https://github.com/Chocobozzz/PeerTube/issues/147 for a list of known
untested equivalents to s3fs.

Now set the base public url of your bucket in your reverse proxy, as shown in the
[official Nginx template](https://github.com/Chocobozzz/PeerTube/blob/be6343d26ec07fd792de069229bd3be27e72d129/support/nginx/peertube#L155-L161).
Videos should now stream from the configured bucket.
